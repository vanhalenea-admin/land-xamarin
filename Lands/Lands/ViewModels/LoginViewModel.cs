﻿using GalaSoft.MvvmLight.Command;
using Lands.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Lands.ViewModels
{
   public  class LoginViewModel : BaseViewModel
    {

        #region Props

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged(() => this.Email);
            }
        }


        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged(() => this.Password);
            }
        }


        private bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                RaisePropertyChanged(() => this.IsRunning);
            }
        }


        public bool IsRemember { get; set; }
  

        #region Commands

        public ICommand LoginCommand {
            get
            {
                return new RelayCommand(Login);
            }
        }
        public ICommand RegisterCommand { get; set; }
        


        #endregion


        #endregion

        public async void Login()
        {
            this.IsRunning = true;
            if (String.IsNullOrEmpty(this.Email))
            {
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Alerta",
                    "Falta ingresar email",
                    "Ok");
                this.IsRunning = true;
            }
            else if (String.IsNullOrEmpty(this.Password))
            {
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Alerta",
                    "Falta ingresar password",
                    "Ok");
                this.IsRunning = true;
            }

            else if (this.Email != "fabia.rojas.a@gmail.com " && this.Password != "1234")
            {
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert(
                "Alerta",
                "Email o password erroneo.",
                "Ok");
                this.Password = String.Empty;
                this.IsRunning = true;
            }
            else
            {  


                var mainViewModel = MainViewModel.GetInstance();  
                mainViewModel.Lands = new LandsViewModel();

                await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());
            }

            this.IsRunning = false;

            return;

        }

        public LoginViewModel()
        {
            this.IsRemember = true;
            this.Email = "fabia.rojas.a@gmail.com";
            this.Password = "1234";

        }







    }
}
