﻿using GalaSoft.MvvmLight.Command;
using Lands.Common;
using Lands.Models;
using Lands.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using Lands.Views;

namespace Lands.ViewModels
{
    public class LandsViewModel : BaseViewModel
    {

        private ApiService _apiService;

        public List<LandModel> listaRespaldo;

        public List<LandModelListView> listaListViewRespaldo;

        private ObservableCollection<LandModelListView> _countries;

        public ObservableCollection<LandModelListView> Countries
        {
            get { return _countries; }
            set
            {
                _countries = value;
                RaisePropertyChanged(() => this.Countries);
            }
        }

        private bool _isRefreshing;

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                RaisePropertyChanged(() => this.IsRefreshing);
            }
        }

        private string _filter;
        public string Filter
        {
            get { return this._filter; }
            set
            {
                _filter = value;
                RaisePropertyChanged(() => this.Filter);

              
                this.Search();
            }
        }

        #region Commands
        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadLands);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }

        public ICommand SelectLandCommand
        {
            get
            {
                return new RelayCommand<object>(SelectLand);
            }
        }


        private async void SelectLand(object param)
        {
            var Land = param as LandModelListView;

            var landModel = this.listaRespaldo.Where(t => t.Alpha2Code == Land.Alpha2Code).First();

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Land = new LandPageViewModel(landModel);

            await Application.Current.MainPage.Navigation.PushAsync(new LandTabedPage());



        }


        private void Search()
        {
            
            if (string.IsNullOrEmpty(this.Filter))
            {
                this.Countries = new ObservableCollection<LandModelListView>(
                    this.listaListViewRespaldo);
            }
            else
            {
                    this.Countries = new ObservableCollection<LandModelListView>(
                    this.listaListViewRespaldo.Where(
                        l => l.Name.ToLower().Contains(this.Filter.ToLower()) ||
                             l.Capital.ToLower().Contains(this.Filter.ToLower())));
            }

        }
        #endregion






        public LandsViewModel()
        {
            this._apiService = new ApiService();
            this.LoadLands();
        }

        private async void LoadLands()
        {
            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                 "Error",
                 connection.Message,
                 "Ok");

                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            this.IsRefreshing = true;

                var response = await this._apiService.GetList<LandModel>(Constants.URL_COUNTRIES, "/rest", "/v2/all");

            this.IsRefreshing = false;
            if(!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Ok");
                return;
            }

            this.listaRespaldo = (List<LandModel>) response.Result;

            var listaListView = new List<LandModelListView>();

            foreach (var obj in this.listaRespaldo)
            {
                LandModelListView listaObj = new LandModelListView();
                listaObj.Capital = obj.Capital;
                listaObj.Name = obj.Name;
                listaObj.Flag = obj.Alpha2Code.ToLower() + ".png";   //obj.Flag;
                listaObj.Alpha2Code = obj.Alpha2Code;
                listaListView.Add(listaObj);
            }

            listaListViewRespaldo = listaListView;

            this.Countries = new ObservableCollection<LandModelListView>(listaListView);
            this.Filter = String.Empty;

        }
    }
}
