﻿using System;
using Lands.Models;
using Xamarin.Forms;

namespace Lands.ViewModels
{
    public class LandPageViewModel : BaseViewModel
    {

        public LandModel Country { get; set; }

        public LandPageViewModel(LandModel land)
        {
            this.Country = land;
        }
    }
}

