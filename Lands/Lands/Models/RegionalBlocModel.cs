﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.Models
{
    public class RegionalBlocModel
    {
        [JsonProperty(PropertyName = "acronym")]
        public string acronym { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }


    }
}
