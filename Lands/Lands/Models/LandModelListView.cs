﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.Models
{
    public class LandModelListView
    {
        public string Name { get; set; }
        public string Capital { get; set; }
        public string Flag { get; set; }
        public string Alpha2Code { get; set; }


    }
}
